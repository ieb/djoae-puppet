#!/usr/bin/python
import base64
from hashlib import sha1
import datetime
import time
import hmac
import sys
import urllib

accessKeyId=sys.argv[1]
secretAccessKey=sys.argv[2]
ttl=int(sys.argv[3])
url=sys.argv[4]
timenow = int(time.mktime(datetime.datetime.now().timetuple()))
expires = timenow+ttl


toSign = "GET\n\n\n%s\n%s" % ( expires, url ) 
hmacDigester = hmac.new(secretAccessKey,toSign.encode("UTF-8"),sha1) 
params = urllib.urlencode({
       "AWSAccessKeyId" : accessKeyId,
       "Expires" : expires,
       "Signature" : base64.b64encode(hmacDigester.digest())})
print 'https://s3.amazonaws.com%s?%s' % ( url, params )

