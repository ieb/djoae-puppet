class pip {
   
    class { python: }
 
    package {
       'python-pip':
          ensure => installed;
    }

}
