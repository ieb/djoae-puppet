#
# Simple DjOAE setup on a single node, with local postgres DB
#

node 'ubuntu.localdomain' {
   Exec {
     path => '/usr/bin:/usr/sbin:/bin:/sbin:/usr/local/bin:/usr/local/sbin',
   }

   class {  'localconfig::hosts': }
   class {  'localconfig': }


   # Apache Config
   class { 'apache': }
   apache::module { 'headers': }
   apache::module { 'expires': }

   # Add a postgres DB and create a user
   class { 'postgres': }

   postgres::database { $localconfig::db_name:
        ensure => present,
   }

   postgres::role { $localconfig::db_user:
        ensure   => present,
        password => $localconfig::db_password,
        require  => Postgres::Database[$localconfig::db_name],
   }
  
   # Add elastic search
   class { 'elasticsearch': }

   class { 'django': }
 
   class {'djoae': }

   djoae::install { $localconfig::http_hostname: 
            db_name => $localconfig::db_name,
            db_user => $localconfig::db_user,
            db_password => $localconfig::db_password,
            db_host => $localconfig::db_host,
            db_port => $localconfig::db_port,
            require => [
                 Postgres::Database["$localconfig::db_name"],
                 Postgres::Role["$localconfig::db_user"],
                 Class['django']
                 ];
   }

      
}
