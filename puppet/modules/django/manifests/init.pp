class django {
            
    class { pip: }
    package {
       python-lxml :
           ensure => installed;
       python-psycopg2 :
           ensure => installed;
    }
    pip::install {
        'django14':
           version => 'Django==1.4';
        'south074':
           version => 'South==0.7.4';
        'oauthlib013' :
           version => 'oauthlib==0.1.3';
        'pyasn1013' :
           version => 'pyasn1==0.1.3';
        'requests0121' :
           version => 'requests==0.12.1';
        'rsa301' :
           version => 'rsa==3.0.1';
        'topia.termextract110' :
           version => 'topia.termextract==1.1.0';
        'django-haystack200beta' :
           version => 'django-haystack==2.0.0-beta',
           source => 'https://s3.amazonaws.com/DjOAEdeps/python/django-haystack-2.0.0-beta.tar.gz';
        'pyelasticsearch005' :
           version => 'pyelasticsearch==0.0.5',
           source => 'https://s3.amazonaws.com/DjOAEdeps/python/pyelasticsearch-0.0.5.tar.gz';
        'jonny-cache14pre1' :
           version => 'johnny-cache==1.4-pre1',
           source => 'https://s3.amazonaws.com/DjOAEdeps/python/johnny-cache-1.4-pre1.tar.gz';
    }

}
