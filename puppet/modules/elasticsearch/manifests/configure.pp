define elasticsearch::configure (
  $clustername = 'caret-es1',
  $clusternetwork = '192.168.100.0/24',
  $version = '0.19.4',
  ) {
  elasticsearch::install {
    'elastic_search_install':
    version => $version;
  }

  file {
    '/etc/elasticsearch':
      ensure => 'directory',
      mode   => '0755';
    '/etc/elasticsearch/elasticsearch.yml':
      content => template(
        'etc/elasticsearch/elasticsearch.yml.erb'
    ),
      owner  => 'root',
      group  => 'elasticsearch',
      mode   => '0640';
    '/etc/elasticsearch/logging.yml':
      content => template(
        'etc/elasticsearch/logging.yml.erb'
    ),
      owner  => 'root',
      group  => 'elasticsearch',
      mode   => '0640';
    '/etc/default/elasticsearch':
      content => template(
        'etc/default/elasticsearch.erb'
    ),
      owner  => 'root',
      group  => 'elasticsearch',
      mode   => '0640';
    '/var/log/elasticsearch':
      ensure => 'directory',
      owner  => 'elasticsearch',
      group  => 'elasticsearch',
      mode   => '0750';
    '/var/lib/elasticsearch':
      ensure => 'directory',
      owner  => 'elasticsearch',
      group  => 'elasticsearch',
      mode   => '0750';
    '/etc/init.d/elasticsearch' :
      source => 'etc/init.d/elasticsearch',
      owner  => 'root',
      group  => 'root',
      mode   => '0754';
  }

  mod_iptables::iptables_open_port { 'elasticsearch_ip_ports':
    portnum   => '9200:9400',
    sourceip  => $clusternetwork,
  }


  service { 'elasticsearch':
    ensure     => 'running',
    enable     => true,
    hasrestart => true,
    hasstatus  => true,
  }

  group { 'elasticsearch' :
    ensure => 'present',
    name   => 'elasticsearch',
  }

  user { 'elasticsearch':
    ensure  => 'present',
    comment => 'elasticsearch User',
    gid     => 'elasticsearch',
    home    => '/usr/share/elasticsearch',
  }
}
