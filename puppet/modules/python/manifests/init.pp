class python {
    package {
         'python' : 
             ensure => installed;
         'python-setuptools' :
             ensure => installed;
    }
    case $::operatingsystem {
      Debian,Ubuntu: {
        package {
         'python-dev' :
             ensure => installed;
        }
      }
      RedHat,CentOS,Linux,Amazon: {
        package {
         'python-devel' :
             ensure => installed;
        }
      }
    default: { notice "Unsupported operatingsystem ${operatingsystem}" }
    }

}
