
This directory contains cloud formation stack configuations that initialise puppet and pull this git repo into the instance.
Some configurations may require access to the git repo via a ssh key stored in S3. To make that possible the 
formation operation needs to be provided with a signature URL that grants access for a limited time. To generate that use the generate-s3-url.py utility

generate-s3-url.py  <AWSAcessKeyID> <AWSAccessKeySecret> <TTL> <URL>

this can be incoporated in the cfn create stack command as below where UIYWEYIU78979 is the AWSAcessKeyID, JoKIUNAi29jNJsdk09oij(iosdkjl is the AWSAccessKeySecret, the url is valid for 600s and the s3 bucket url is /DjOAEdeps/keys/puppet-ssh-key

cfn-create-stack TestPuppetStack8 --template-file simple1nodepuppet.json -p \
   "KeyName=awslaunch-201006;DeployKeyUrl=`./generate-s3-url.py UIYWEYIU78979 JoKIUNAi29jNJsdk09oij(iosdkjl 600 /DjOAEdeps/keys/puppet-ssh-key`"

Obviously, the AWS IDs are not real.
Note: the server time may be out relative to you local box, and it can take time for the S3 instance to boot.
