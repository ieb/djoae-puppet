define djoae::install(
      $db_name = "djoae",
      $db_user = "djoae",
      $db_password = "djoaepass",
      $db_host = "localhost",
      $db_port = "5432"
    ) {

    $source = "/var/www/$name"
    include apache


    package {
      'libapache2-mod-wsgi' :
         ensure => present;
      'imagemagick' :
	 ensure => present;
      'xpdf' :
	 ensure => present;
      'default-jdk' :
         ensure => present;
    }

    exec {
      "$name checkout-djoe" :
          command => "git clone git@bitbucket.org:ieb/djoae.git $source",
          notify  => File["$source"],
          creates => "$source/.git";
      "$name branch-deployment" :
          command => "git checkout -b deployment origin/deployment",
          cwd => $source,
          onlyif => "git branch | grep -c '* deployment' | grep 0",
          notify  => File["$source"],
          require => Exec["$name checkout-djoe"];
      "$name clean-branch" :
          command => "git checkout .",
          cwd => $source,
          notify  => File["$source"],
          require => Exec["$name branch-deployment"];
      "$name branch-pull" :
         command => "git pull origin deployment",
         cwd => $source,
         logoutput => true,
         notify  => File["$source"],
         require => Exec["$name clean-branch"];
      "$name mark-deploy" :
         command => "git log | head -4  > deploy.txt",
         cwd => $source,
         notify  => File["$source"],
         require => Exec["$name branch-pull"];
      "$name add-status" :
         command => "git status >> deploy.txt",
         returns => [0,1],
         cwd => $source,
         notify  => File["$source"],
         require => Exec["$name mark-deploy"];
      "$name mark-version" :
        command => "git show --pretty=oneline HEAD | cut -c1-41 | head -1  > version.txt",
        cwd => $source,
        notify  => File["$source"],
        require => Exec["$name branch-pull"];
      "$name extract-deps" :
        command => "curl https://s3.amazonaws.com/DjOAEdeps/preview-deps.tar.gz | tar xzf - ",
        cwd => "$source/app/oae/content/preview/deps",
        creates => "$source/app/oae/content/preview/deps/tika-app-1.1.jar",
        require => Exec["$name branch-pull"];
      "$name extract-ui" :
        command => "curl https://s3.amazonaws.com/DjOAEdeps/oae-ui-1.3.0.tar.gz | tar xzf - ",
        cwd => "$source/static-data/ui",
        creates => "$source/static-data/ui/version.txt",
        require => [Exec["$name branch-pull"],File["$source/static-data/ui"]];
      "${name} disable default":
         command => "a2dissite default",
         onlyif  => "test -L ${apache::params::conf}/sites-enabled/000-default",
         notify  => Exec["apache-graceful"],
         require => Package["apache"];
      "${name} enable-site":
         command => "a2ensite $name",
         unless  => "test -L ${apache::params::conf}/sites-enabled/$name",
         require => Exec["$name disable default"],
         notify => Exec["apache-graceful"];
    }

    file {
      "$source/static-data/ui":
         ensure => directory;
      "$source/app/oae/local_settings.py" : 
        content => template("djoae/local_settings.py.erb"),
        ensure => present,
        notify  => File["$source"],
        require => Exec["$name branch-pull"];
     "${apache::params::conf}/sites-available/$name":
        content => template("djoae/httpd.conf.erb"),
        require => File["$source/app/oae/local_settings.py"],
        notify => Exec["$name enable-site"];
     "$source" :
        owner => "root",
        group => "root",
        recurse => true;
     "$source/app-data" :
        owner => "www-data",
        group => "root",
        recurse => true,
        require => File["$source"];
    }
    
    

    exec {
      "$name validate" :
        command => "python manage.py validate",
        cwd => "$source/app/oae",
        require => File["$source/app/oae/local_settings.py"];
      "$name check_dependencies" :
        command => "python manage.py validate",
        cwd => "$source/app/oae",
        require => Exec["$name validate"];
      "$name syncdb_and_migrate" :
        command => "python manage.py syncdb --noinput && python manage.py migrate",
        cwd => "$source/app/oae",
        logoutput => true,
        require => Exec["$name check_dependencies"],
        notify => Exec["apache-graceful"];
      "$name collectstatic" :
        command => "python manage.py collectstatic --noinput -v 0",
        cwd => "$source/app/oae",
        notify  => File["$source"],
        require => Exec["$name validate"];
    }

}
