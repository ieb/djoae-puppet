#!/bin/sh
# Do this first 
#  cd /etc/puppet
#  git clone git@bitbucket.org:ieb/djoae-puppet.git
#  sh djoae-puppet/prepare.sh
#
rm -rf modules
ln -s djoae-puppet/puppet/modules/ .
cd djoae-puppet/puppet/modules/
ln -s ../environments/test1/localconfig .
cd ../../
git submodule update --init
puppet apply --verbose puppet/nodes/standalone.pp
