define pip::install (
   $version = '',
   $source = ''
   ) {
    $installspec = $source ? {
        '' => $version,
        default => $source
    }
    case $::operatingsystem {
       Debian,Ubuntu: {
          $pipcmd = 'pip'
       }
       RedHat,CentOS,Linux,Amazon: {
         $pipcmd = 'pip-python'
       }
       default: { notice "Unsupported operatingsystem ${operatingsystem}" }
    }

    exec {
      "pipinstall-$version": 
          command => "$pipcmd install $installspec",
          onlyif => "$pipcmd freeze | grep -c $version | grep -q 0",
          require => Package['python-pip'];
    }
}
