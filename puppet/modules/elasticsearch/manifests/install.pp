define elasticsearch::install (
  $version = '0.19.4',
) {

  exec { 'fetch-package':
    command     => "curl --silent https://s3.amazonaws.com/DjOAEdeps/elasticsearch-caret-$version.tar.gz > /tmp/elasticsearch.tar.gz",
    unless      => 'ls /usr/share/elasticsearch',
    notify      => Exec['unpack-es-package'],
  }

  exec { 'unpack-es-package':
    command     => 'tar -x -z --group=root --owner=root -C / -f /tmp/elasticsearch.tar.gz',
    refreshonly => true,
    notify      => Exec['clean-es-package'],
  }

  exec { 'clean-es-package':
    command     => 'rm -f /tmp/elasticsearch.tar.gz',
    refreshonly => true,
  }
}

